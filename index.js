/*globals require, exports */

exports.LinearRegression = require('./lib/linear_regression').LinearRegression;
exports.euclideanDistance = require('./lib/distance').euclidean;
